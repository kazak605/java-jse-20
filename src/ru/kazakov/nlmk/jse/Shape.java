package ru.kazakov.nlmk.jse;

public abstract class Shape {

    public abstract double getArea();

}
