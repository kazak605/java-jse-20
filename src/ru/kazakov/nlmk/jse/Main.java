package ru.kazakov.nlmk.jse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Main {

    public static void main(String[] args) {
        //
        Circle circle1 = new Circle(5);
        Circle circle2 = new Circle(10);
        Circle circle3 = new Circle(15);
        Rectangle rectangle1 = new Rectangle(2, 4);
        Rectangle rectangle2 = new Rectangle(4, 8);
        Rectangle rectangle3 = new Rectangle(6, 12);
        Square square1 = new Square(11);
        Square square2 = new Square(22);
        Square square3 = new Square(33);
        //
        Collection<Circle> circles = new ArrayList<>();
        addShapes(circles, new Circle[] {circle1, circle2, circle3});
        System.out.println("Суммарная площадь кругов: " + getArea(circles));
        System.out.println();
        Collection<Rectangle> rectangles = new ArrayList<>();
        addShapes(rectangles, new Rectangle[]{rectangle1, rectangle2, rectangle3});
        System.out.println("Суммарная площадь треугольников: " + getArea(rectangles));
        System.out.println();
        Collection<Square> squares = new ArrayList<>();
        addShapes(squares, new Square[]{square1, square2, square3});
        System.out.println("Суммарная площадь квадратов: " + getArea(squares));
        System.out.println();
        Collection<Shape> shapes = new ArrayList<>();
        addShapes(shapes, new Shape[]{circle1, rectangle1, square3});
        System.out.println("Суммарная площадь форм: " + getArea(shapes));

    }

    public static <T extends Shape> void addShapes(Collection<T> collection, T...shapes) {
        collection.addAll(Arrays.asList(shapes));
    }

    public static int getArea(Collection<? extends Shape> shapes) {
        int sum = 0;
        for (Shape shape : shapes) {
            sum += (shape).getArea();
        }
        return sum;
    }

}
