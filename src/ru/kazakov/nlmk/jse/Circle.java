package ru.kazakov.nlmk.jse;

public class Circle extends Shape {

    double length;

    public Circle(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return Math.PI * (this.length * this.length);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "length=" + length +
                '}';
    }
}
